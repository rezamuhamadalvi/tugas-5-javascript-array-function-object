var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"].sort();   //soal 1
for (let i=0; i < daftarHewan.length; i++) {    //Jawaban soal 1
    console.log(daftarHewan[i])
  }

function introduce(data) {    //soal 2
  //jawaban soal 2
    var nama = data.name
    var umur = data.age
    var alamat = data.address
    var hobby = data.hobby
    var hasil = "Nama saya "+nama+", umur saya "+umur+" Tahun, alamat saya di "+alamat+", dan saya punya hobby yaitu "+hobby+" !"
    console.log(hasil)
  }
 var data = {name : "Reza Muhamad Alvi" , age : 19 , address : "Jalan Pangeran Antasari Blok Lebak Jambu RT/RW 003/002 Keluarahan Kenanga Kecamatan Sumber Kabupaten Cirebon" , hobby : "Beladiri" }
 var perkenalan =  introduce(data); 
 console.log(perkenalan);

function hitung_huruf_vokal(str) {   //soal 3 
//jawaban soal 3
    var count = str.match(/[aeiou]/gi).length;
    return count;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

 function deret(angka) {   //soal 4
    return angka * 2 - 2
  }
   //jawaban soal 4
  console.log( deret(0) ) // -2
  console.log( deret(1) ) // 0
  console.log( deret(2) ) // 2
  console.log( deret(3) ) // 4
  console.log( deret(5) ) // 8